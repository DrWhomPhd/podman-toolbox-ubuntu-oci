# podman-toolbox-ubuntu-oci

To use with `toolbox`:

```
toolbox create --image registry.gitlab.com/drwhomphd/podman-toolbox-ubuntu-oci:main
```

This will prompt you to download the image and then will automatically create the toolbox. 

To update image:
Use podman to stop running containers. Then:
```
podman pull registry.gitlab.com/drwhomphd/podman-toolbox-ubuntu-oci:main
```

Containerfile will list all packages installed in this container image.

Currently focuses on Ubuntu 20.04.
